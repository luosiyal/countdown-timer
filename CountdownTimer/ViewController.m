//
//  ViewController.m
//  CountdownTimer
//
//  Created by Billy Luo on 10/22/14.
//  Copyright (c) 2014 Billy Luo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(void) timerRun {
    secondsCount = secondsCount - 1;
    int minuts = secondsCount / 60;
    int seconds = secondsCount - (minuts * 60);
    
    NSString *timerOutput = [NSString stringWithFormat:@"%2d:%.2d", minuts, seconds];
    countdownLabel.text = timerOutput;
    
    if (secondsCount == 0) {
        [countdownTimer invalidate];
        countdownTimer = nil;
    }
    
}

-(void) setTimer {
    secondsCount = 30;
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerRun) userInfo:nil repeats:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
