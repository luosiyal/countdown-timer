//
//  main.m
//  CountdownTimer
//
//  Created by Billy Luo on 10/22/14.
//  Copyright (c) 2014 Billy Luo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
