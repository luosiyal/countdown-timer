//
//  ViewController.h
//  CountdownTimer
//
//  Created by Billy Luo on 10/22/14.
//  Copyright (c) 2014 Billy Luo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UILabel *countdownLabel;
    NSTimer *countdownTimer;
    int secondsCount;
}
@end
